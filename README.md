# Ansible role Apache

Simple Ansible role to install and configure Apache in my self-hosting context

The variable `apache_remoteip_internal_proxies` needs to be set with the list of
IP addresses of reverse-proxies.

Example :
```
  vars:
    apache_remoteip_internal_proxies:
      - 192.168.0.10
      - 192.168.0.11
```